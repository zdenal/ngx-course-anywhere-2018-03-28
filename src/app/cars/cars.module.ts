import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ValidatorsModule } from 'ngx-validators';

import { ValidationModule } from './../validation/validation.module';
import { CarsApiFirebaseService } from './cars-api-firebase.service';
import { CarsApiService } from './cars-api.service';
import { CarsComponent } from './cars.component';
import { CarsService } from './cars.service';
import { CreateComponent } from './create/create.component';
import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { TemplateFormComponent } from './template-form/template-form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule,
    ValidatorsModule
  ],
  providers: [
    {
      provide: CarsApiService,
      useClass: CarsApiFirebaseService
    },
    CarsService
  ],
  declarations: [
    CarsComponent,
    ListComponent,
    DetailComponent,
    CreateComponent,
    TemplateFormComponent,
    ReactiveFormComponent
  ]
})
export class CarsModule {}
