import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Car, Cars } from './../cars.model';
import { CarsService } from './../cars.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cars-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit {

  i18NPrefix = 'cars.list.';

  state$: Observable<{
    cars: Cars;
    loadingCars: boolean;
    carsError: boolean;
    selectedCar: Car
  }>;

  constructor(
    private carsService: CarsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.state$ = this.carsService.state$.map((state) => {
      return {
        cars: state.cars,
        loadingCars: state.loadingCars && state.firstLoadingCars,
        carsError: state.carsError,
        selectedCar: state.selectedCar
      };
    });
  }

  ngOnInit() {
    this.carsService.loadCars();
  }

  onCarClick(car: Car): void {
    this.router.navigate(['detail', car.id], { relativeTo: this.route });
  }

  onCreateCar(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
