import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { CarsApiService } from './cars-api.service';
import { Car, Cars, CarsState } from './cars.model';

@Injectable()
export class CarsService {

  state$: Observable<CarsState>;

  private stateSource = new BehaviorSubject<CarsState>({
    cars: [],
    selectedCar: null,
    loadingCars: false,
    firstLoadingCars: true,
    carsError: false
  });

  constructor(
    private api: CarsApiService
  ) {
    this.state$ = this.stateSource.asObservable();
  }

  loadCars(): void {
    this.updateState((state) => {
      state.loadingCars = true;
      state.carsError = false;
    });
    this.api.getCars$().first().subscribe((cars) => {
      this.updateState((state) => {
        state.cars = cars;
        state.loadingCars = false;
        state.firstLoadingCars = false;
      });
    }, (error) => {
      console.error(error);
      this.updateState((state) => {
        state.loadingCars = false;
        state.firstLoadingCars = false;
        state.carsError = true;
      });
    });
  }

  selectCar(car: Car): void {
    this.updateState((state) => {
      state.selectedCar = car;
    });
  }

  deselectCar(): void {
    this.selectCar(null);
  }

  getCar$(id: string): Observable<Car> {
    return this.state$.first().mergeMap((state) => {
      const car = (state.selectedCar && state.selectedCar.id === id) ? state.selectedCar : state.cars.find((c) => c.id === id);
      if (car) {
        return Observable.of(car);
      } else {
        return this.api.getCar$(id);
      }
    });
  }

  updateCar$(car: Car): Observable<Car> {
    return this.api.updateCar$(car).do((updatedCar) => {
      this.updateState((state) => {
        const idx = state.cars.findIndex((c) => c.id === updatedCar.id);
        if (idx > -1) {
          state.cars[idx] = updatedCar;
        }
        if (state.selectedCar && state.selectedCar.id === updatedCar.id) {
          state.selectedCar = updatedCar;
        }
      });
      this.loadCars();
    });
  }

  createCar$(car: Car): Observable<Car> {
    return this.api.createCar$(car).do((createdCar) => {
      this.updateState((state) => {
        state.selectedCar = createdCar;
      });
      this.loadCars();
    });
  }

  deleteCar$(id: string): Observable<any> {
    return this.api.deleteCar$(id).do(() => {
      this.deselectCar();
      this.loadCars();
    });
  }

  private updateState(change: (state: CarsState) => void): void {
    const value = this.stateSource.getValue();
    change(value);
    this.stateSource.next(value);
  }

}
