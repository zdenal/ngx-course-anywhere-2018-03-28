export type Fuel = 'diesel' | 'petrol' | 'hybrid';

export interface CarData {
  brand: string;
  model: string;
  fuel: Fuel;
  power: number;
  description?: string;
}

export interface Car extends CarData {
  id: string;
}

export type Cars = Car[];

export interface CarsConfig {
  getCarsUrl: string;
  createCarUrl: string;
  getCarUrl: string;
  updateCarUrl: string;
  deleteCarUrl: string;
}

export interface CarsResponse {
  [id: string]: CarData;
}

export interface CarsState {
  cars: Cars;
  selectedCar: Car;
  loadingCars: boolean;
  firstLoadingCars: boolean;
  carsError: boolean;
}
