import { HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppStartupService } from './app-startup.service';
import { AppComponent } from './app.component';
import { appConfigValue, carsConfigValue } from './app.config';
import { appConfigToken } from './app.di';
import { AppConfig } from './app.model';
import { carsConfigToken } from './cars/cars.di';
import { CarsModule } from './cars/cars.module';
import { HeaderComponent } from './header/header.component';
import { HttpStatus404Component } from './http-status-404/http-status-404.component';

export function translateLoaderFactory(http: HttpClient, appConfig: AppConfig) {
  return new TranslateHttpLoader(http as any, './assets/i18n/', `.json?version=${appConfig.version}`);
}

export function initializerFactory(startupService: AppStartupService) {
  return () => startupService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HttpStatus404Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: translateLoaderFactory,
          deps: [HttpClient, appConfigToken]
      }
    }),
    AppRoutingModule
  ],
  providers: [
    {
      provide: appConfigToken,
      useValue: appConfigValue
    },
    {
      provide: carsConfigToken,
      useValue: carsConfigValue
    },
    AppStartupService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializerFactory,
      deps: [AppStartupService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
