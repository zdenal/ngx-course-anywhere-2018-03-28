import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ContactComponent } from './contact.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [ContactComponent]
})
export class ContactModule { }
