export function isDefined(arg: any): boolean {
  return arg !== null && typeof arg !== 'undefined';
}

export function isNotDefined(arg: any): boolean {
  return !isDefined(arg);
}

export function isEmpty(val: string | number | boolean) {
  return isNotDefined(val) || (typeof val === 'string' && val.length === 0);
}

export function interpolate(template: string, values: Object) {
  return template.replace(/#{([\w0-9]+)}/g, (val, match) => {
    return isEmpty(values[match]) ? val : values[match];
  });
}

export function assign(target, ...sources) {
  return Object.assign({}, target, ...sources);
}
